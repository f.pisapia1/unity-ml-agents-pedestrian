using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Curva22Environment : MonoBehaviour
{
    public RLAgent agent;
    public GameObject target;
    void Start()
    {
        agent.agentTerminated += TargetChangePosition;
    }
    public void TargetChangePosition(float obj, Environment env)
    {
        int randomMultiplier = Random.Range(0, 2) * 2 - 1;
        Vector3 newPosition = target.transform.localPosition;
        newPosition.z = randomMultiplier * 8.5f;
        target.transform.localPosition = newPosition;

        Vector3 newPositionAgent = agent.transform.localPosition;
        newPositionAgent.z = - (randomMultiplier * 8.75f);
        agent.transform.localPosition = newPositionAgent;

        //Quaternion newRotationAgent = agent.transform.rotation;
        //agent.transform.localEulerAngles = new Vector3(0, (randomMultiplier * 90f), 0);


    }
}
