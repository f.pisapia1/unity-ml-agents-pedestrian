using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class OsservaEnvironment : MonoBehaviour
{
    public GameObject[] FinalTarget;
    public RLAgent agent;


    public float intervalloAccensione = 2.0f;

    private int indiceOggettoCorrente = -1;
    void Start()
    {
        agent.agentTerminated += AccendiOggettoCasuale;
        AccendiOggettoCasuale(0f , null);
        //InvokeRepeating("AccendiOggettoCasuale", intervalloAccensione, intervalloAccensione);

    }

    private void AccendiOggettoCasuale(float obj, Environment env)
    {

        if (indiceOggettoCorrente != -1)
            FinalTarget[indiceOggettoCorrente].SetActive(false);

        int nuovoIndice = Random.Range(0, FinalTarget.Length);
        FinalTarget[nuovoIndice].SetActive(true);

        indiceOggettoCorrente = nuovoIndice;
    }


}
