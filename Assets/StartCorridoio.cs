using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCorridoio : MonoBehaviour
{
    public RLAgent agent;
    public GameObject target;
    void Start()
    {
        agent.agentTerminated += TargetChangePosition;
    }
    public void TargetChangePosition(float obj, Environment env)
    {
        int randomMultiplier = Random.Range(0, 2) * 2 - 1;
        Vector3 newPosition = target.transform.localPosition;
        newPosition.z = randomMultiplier * 8.3f;
        target.transform.localPosition = newPosition;
    }
}
