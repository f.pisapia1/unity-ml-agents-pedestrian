using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class StartEnvironment : MonoBehaviour
{
    public RLAgent agent;
    public GameObject finalTarget;

    private float rangeXMin = -8.5f;
    private float rangeXMax = 8.5f;
    private float rangeZMin = -8.5f;
    private float rangeZMax = 8.5f;

    // Start is called before the first frame update
    void Start()
    {
        agent.agentTerminated += shiftEnvObj;

    }

    private void shiftEnvObj(float obj, Environment env)
    {
        //finale target position
        float posX = Random.Range(rangeXMin, rangeXMax);
        float posZ = Random.Range(rangeZMin, rangeZMax);
        //agent position 
        float agentX = Random.Range(rangeXMin, rangeXMax);
        float agentZ = Random.Range(rangeZMin, rangeZMax);

        Vector3 nuovaPosizione = new Vector3(posX, finalTarget.transform.position.y, posZ);
        Vector3 nuovaPosizioneAgent = new Vector3(agentX, finalTarget.transform.position.y, agentZ);
        finalTarget.transform.localPosition = nuovaPosizione;
        agent.transform.localPosition = nuovaPosizioneAgent;
        agent.transform.localEulerAngles = new Vector3(0, Random.Range(0f, 360f), 0);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
