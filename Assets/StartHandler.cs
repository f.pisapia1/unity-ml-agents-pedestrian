using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartHandler : MonoBehaviour
{
    public GameObject StartUI;
    private bool start;
    private GameObject instantiatedEnvironment;
    private void Awake()
    {
        start = true;
    }
    public void OnClick()
    {
        var env = DropdownHandler.selectedEnv;
        if (start)
        {
            instantiatedEnvironment = Instantiate(env, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
            transform.GetComponentInChildren<Text>().text = "STOP";
        }
        else
        {
            Destroy(instantiatedEnvironment);
            transform.GetComponentInChildren<Text>().text = "START";

        }
        StartUI.active = !start;
        start = !start;


    }
}
