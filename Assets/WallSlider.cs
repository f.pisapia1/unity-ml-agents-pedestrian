using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSlider : MonoBehaviour
{
    public RLAgent agent;
    private Vector3 startPosition;
    private void Start()
    {
        agent.agentTerminated += Slider;
        startPosition = transform.position;

    }
    private void  Slider(float _, Environment _1)
    {
        float pos = Random.Range(startPosition.x - 0.3f, startPosition.x + 0.3f);
        transform.position = new Vector3(pos, 0, startPosition.z);
    }
}
