## Requisiti Necessari
- Unity Version 2022.3.3f1
- Unity Hub 
- Python :
	-mlagents                0.25.0
	-mlagents-envs           0.25.0
	-numpy                   1.19.5
	-tensorboard             2.14.0
	-tensorboard-data-server 0.7.1
	-torch                   1.7.1+cpu 
	resto delle librerie da installare lo trovate in requirements
- package da aggiungere a unity: Ml Agents 1.0.8 (https://docs.unity3d.com/Packages/com.unity.ml-agents@1.0/manual/index.html) la versione 2.0.1 richiede modifiche al codice


## Installazione Unity
- Installare Unity Hub dal link: https://unity3d.com/get-unity/download
- Dalle impostazioni andare su _License Management_ e attivare la licenza (ad esempio tramite mail universitaria), poi tornare indietro nella schermata principale
- Nella sezione _Installs_ cliccare sul bottone _Add_ e installare la versione richiesta, se questa non si trova cliccare su _download archives_ e cercarla

## Apertura repo
- Clonare la repository (Ad esempio con GitHubDesktop) e scegliere il branch master
- Selezionare poi la versione di Unity richiesta
- Aprire il progetto

##Scene
la scena da utilizzare e' Refactoring

##Comandi utili
aprire il cmd da dentro la cartella del progetto
per eseguire un Training -> mlagents-learn TrainerConfig/trainer_config.yaml --run-id=....
per riprendere un Training da un checkPoint -> mlagents-learn TrainerConfig/trainer_config.yaml --run-id=... --resume --run-id=MyRun --initialize-from=...   (es:Models\BASE\Pedone\model-906661.ckpt)
visionare i grafici tensorboard -> tensorboard --logdir=results   o   tensorboard --logdir=summaries

## Link Utili:
https://github.com/Unity-Technologies/ml-agents/blob/com.unity.ml-agents_1.0.8/docs/Getting-Started.md
https://github.com/Unity-Technologies/ml-agents/tree/release_16_docs
https://github.com/Unity-Technologies/ml-agents/blob/release_20_docs/docs/ML-Agents-Overview.md
https://unity-technologies.github.io/ml-agents/Training-Configuration-File/
- [Come creare un ambiente](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Creazione-Ambiente)
- [Utilizzo del Gestore Ambiente](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Utilizzo-del-Gestore-Ambiente)
- [Utilizzo del Gestore Curriculum](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Utilizzo-del-Gestore-Curriculum)
- [Come effettuare un Training](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Come-effettuare-il-Training)
- [Come salvare i dati per analizzarli in Python](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/Come-utilizzare-Python)
- [TensorBoard e andamento delle reward](https://gitlab.com/t.albericci1/environment-iniziale/-/wikis/WayFinding/TensorBoard-e-andamento-delle-reward)
